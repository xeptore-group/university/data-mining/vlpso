package config

import (
	"fmt"

	"github.com/spf13/viper"
)

// Configuration represents application typed configurations.
type Configuration struct {
	SU struct {
		Precision int `mapstructure:"precision"`
	} `mapstructure:"su"`
	Swarm struct {
		DivisionsCount          uint    `mapstructure:"divisions_count"`
		ParticlesCount          uint    `mapstructure:"particles_count"`
		ParticleEncodeThreshold float64 `mapstructure:"particle_encode_threshold"`
	} `mapstructure:"swarm"`
	Optimization struct {
		TrainingSetPercentage    uint `mapstructure:"training_set_percentage"`
		KnnKInPercentage         uint `mapstructure:"knn_k_in_percentage"`
		MaxGbestUnchagedCount    uint `mapstructure:"max_gbest_unchaged_count"`
		MaxPbestUnchagedCount    uint `mapstructure:"max_pbest_unchaged_count"`
		PbestUnchangingPrecision uint `mapstructure:"pbest_unchanging_precision"`
		GbestUnchangingPrecision uint `mapstructure:"gbest_unchanging_precision"`
		TotalEvolutionIterations int  `mapstructure:"total_evolution_iterations"`
	} `mapstructure:"optimization"`
}

var c Configuration = Configuration{}

// Initiate reads and store configurations and make it ready to work.
func Initiate() {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.SetConfigType("yml")
	if err := viper.ReadInConfig(); err != nil {
		panic(fmt.Errorf("fatal error config file: %s", err.Error()))
	}
	if err := viper.Unmarshal(&c); err != nil {
		panic(fmt.Errorf("invalid config file format: %s", err.Error()))
	}
	validateConfiguration(&c)
}

func validateConfiguration(c *Configuration) {
	if c.Optimization.GbestUnchangingPrecision == 0 {
		panic("")
	}
}

// Config returns stored configuration instance.
func Config() Configuration {
	return c
}
