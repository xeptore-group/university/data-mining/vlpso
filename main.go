package main

import (
	"bufio"
	"encoding/csv"
	"io"
	"log"
	"os"
	"strconv"

	"gitlab.com/xeptore-group/university/data-minings/vlpso/config"
	"gitlab.com/xeptore-group/university/data-minings/vlpso/optimizer"
	"gitlab.com/xeptore-group/university/data-minings/vlpso/su"
)

func main() {
	config.Initiate()
	cfg := config.Config()

	o := optimizer.New(
		cfg.Optimization.MaxPbestUnchagedCount,
		cfg.Optimization.MaxGbestUnchagedCount,
		cfg.Swarm.DivisionsCount,
		cfg.Swarm.ParticlesCount,
	)

	data := [][]float64{}
	labels := []string{}

	csvFile, _ := os.Open("data.csv")
	reader := csv.NewReader(bufio.NewReader(csvFile))
	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}
		row := make([]float64, 0, len(line))
		for _, section := range line {
			f, _ := strconv.ParseFloat(section, 64)
			row = append(row, f)
		}
		data = append(data, row)
	}

	f, _ := os.Open("label.csv")
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		labels = append(labels, line)
	}

	rearrangedDataset := su.RearrangeDataset(data, labels)
	o.Optimize(rearrangedDataset, labels)
}
