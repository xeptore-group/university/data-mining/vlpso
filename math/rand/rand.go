package rand

import (
	"math/rand"
	"time"
)

// Float64 generates a float64 random number in range [0.0, 1.0)
func Float64() float64 {
	src := rand.NewSource(time.Now().UnixNano())

	return rand.New(src).Float64()
}

// Float64s generates a slice of float64 random numbers in range [0.0, 1.0)
func Float64s(count uint) []float64 {
	out := make([]float64, count)

	for i := uint(0); i < count; i++ {
		out[i] = Float64()
	}

	return out
}

// Int63n generates a random uint64.
func Int63n(n int64) int64 {
	src := rand.NewSource(time.Now().UnixNano())
	return rand.New(src).Int63n(n)
}

// Int32n generates a random uint32.
func Int32n(n int32) int32 {
	src := rand.NewSource(time.Now().UnixNano())
	return rand.New(src).Int31n(n)
}
