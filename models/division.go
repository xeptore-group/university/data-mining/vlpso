package models

import (
	"sync"
)

type division struct {
	index           uint
	particles       []particle
	particlesLength uint
}

func (d *division) updateParticlesFitness(dataset [][]float64, labels []string) {
	wg := new(sync.WaitGroup)
	wg.Add(len(d.particles))
	for i := 0; i < len(d.particles); i++ {
		p := &d.particles[i]
		go func() {
			p.updateFitness(dataset, labels)
			wg.Done()
		}()
	}

	wg.Wait()
}

func (d *division) assignExemplars(possibleDivisions []division) {
	for i := 0; i < len(d.particles); i++ {
		d.particles[i].assignExemplar(d.index, uint(i), possibleDivisions)
	}
}

func (d *division) getAverageFitness() float64 {
	fitnesses := 0.0
	for i := 0; i < len(d.particles); i++ {
		fitnesses += d.particles[i].fitness
	}

	return fitnesses / float64(len(d.particles))
}
