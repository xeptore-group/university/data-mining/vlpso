package models

import (
	"math"

	"gitlab.com/xeptore-group/university/data-minings/knn"
	"gitlab.com/xeptore-group/university/data-minings/vlpso/config"
	"gitlab.com/xeptore-group/university/data-minings/vlpso/math/rand"
)

type position []float64

type particle struct {
	position            position
	velocity            []float64
	exemplars           []exemplarID
	learningProbability float64
	renewExemplarCount  uint
	bestPosition        position
	fitness             float64
	rank                uint
}

type exemplarID struct {
	divisionIndex uint
	particleIndex uint
}

func (p *particle) updateFitness(dataset [][]float64, labels []string) {
	cfg := config.Config()
	encoded := p.encode(cfg.Swarm.ParticleEncodeThreshold)
	filteredDataset := make([][]float64, len(labels))
	for i := 0; i < len(dataset); i++ {
		row := make([]float64, 0, len(encoded))
		for _, idx := range encoded {
			row = append(row, dataset[i][idx])
		}
		filteredDataset[i] = row
	}
	trainSetSize := cfg.Optimization.TrainingSetPercentage * uint(len(filteredDataset)) / 100
	classifier := knn.New(trainSetSize / cfg.Optimization.KnnKInPercentage)
	classifier.Train(filteredDataset[:trainSetSize], labels[:trainSetSize])
	score := classifier.Score(filteredDataset[trainSetSize:], labels[trainSetSize:])
	if score-p.fitness < math.Pow10(-int(cfg.Optimization.PbestUnchangingPrecision)) {
		p.bestPosition = p.position
		p.renewExemplarCount = 0
	} else {
		p.renewExemplarCount++
	}
	p.fitness = score
}

func (p *particle) updateVelocity(divisions []division) {
	newVelocity := make([]float64, len(p.velocity))
	for i := 0; i < len(p.velocity); i++ {
		temp := 0.6 * p.velocity[i]
		temp += 0.4 * rand.Float64() * (divisions[p.exemplars[i].divisionIndex].particles[p.exemplars[i].particleIndex].position[i] - p.position[i])
		newVelocity[i] = temp
	}
	p.velocity = newVelocity
}

func (p *particle) updatePosition() {
	for i := 0; i < len(p.position); i++ {
		p.position[i] = p.position[i] + p.velocity[i]
		if p.position[i] > 1.0 {
			p.position[i] = 1.0
		}
	}
}

func (p *particle) encode(threshold float64) []int {
	out := make([]int, 0, len(p.position))
	for i, pos := range p.position {
		if pos > threshold {
			out = append(out, i)
		}
	}
	out = append(out[0:0:0], out...)

	return out
}

func (p *particle) assignExemplar(divIndex, pIndex uint, possibleDivisions []division) {
	exemplars := make([]exemplarID, len(p.position))
	for j := 0; j < len(p.position); j++ {
		rnd := rand.Float64()
		if rnd > p.learningProbability {
			exemplars[j] = exemplarID{
				divisionIndex: divIndex,
				particleIndex: pIndex,
			}
		} else {
			p1 := pickRandomParticle(
				possibleDivisions,
				divIndex,
			)
			p2 := pickRandomParticle(
				possibleDivisions,
				divIndex,
				randomPickExcluded{divIndex: p1.divIndex, pIndex: uint(p1.pIndex)},
			)

			if possibleDivisions[p1.divIndex].particles[p1.pIndex].fitness > possibleDivisions[p2.divIndex].particles[p2.pIndex].fitness {
				exemplars[j] = exemplarID{
					divisionIndex: possibleDivisions[p1.divIndex].index,
					particleIndex: p1.pIndex,
				}
			} else {
				exemplars[j] = exemplarID{
					divisionIndex: possibleDivisions[p2.divIndex].index,
					particleIndex: p2.pIndex,
				}
			}
		}
	}
	p.exemplars = exemplars
	p.renewExemplarCount = 0
}

type randomPickExcluded struct {
	divIndex uint
	pIndex   uint
}

type randomPickedParticle struct {
	divIndex uint
	pIndex   uint
}

func pickRandomParticle(possibleDivisions []division, minDivIndex uint, excluded ...randomPickExcluded) randomPickedParticle {
	for {
		divRandIndex := uint(rand.Int32n(int32(len(possibleDivisions))))
		if len(possibleDivisions) == 1 {
			divRandIndex = 0
		}
		pRandIndex := uint(rand.Int32n(int32(len(possibleDivisions[divRandIndex].particles))))
		for _, exc := range excluded {
			if exc.divIndex == divRandIndex && exc.pIndex == pRandIndex {
				continue
			}
		}
		if possibleDivisions[divRandIndex].index < minDivIndex && len(possibleDivisions) != 1 {
			continue
		}
		return randomPickedParticle{
			divIndex: divRandIndex,
			pIndex:   pRandIndex,
		}
	}
}
