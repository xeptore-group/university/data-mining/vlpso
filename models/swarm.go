package models

import (
	"log"
	"math"
	"sort"
	"sync"

	"gitlab.com/xeptore-group/university/data-minings/vlpso/config"
	"gitlab.com/xeptore-group/university/data-minings/vlpso/math/rand"
)

// Swarm represents a swarm instance.
// It contains particles and captures best position of the whole population.
type Swarm struct {
	divisions                   []division
	particlesCount              uint
	bestParticle                *particle
	bestUnchangedCount          uint
	maxParticleBestUnchageCount uint
	maxParticlesLength          uint
}

// NewSwarm returns a new instance of Swarm.
func NewSwarm(
	divsCount,
	particlesCount,
	maxParticlesLength,
	maxParticleBestUnchageCount uint) *Swarm {
	return generateSwarm(divsCount, particlesCount, maxParticlesLength, maxParticleBestUnchageCount)
}

func generateSwarm(divsCount, particlesCount, maxParticlesLength, maxParticleBestUnchageCount uint) *Swarm {
	log.Printf("generating swarm. divisions: %d total number of particles: %d", divsCount, particlesCount)
	swarm := &Swarm{
		particlesCount:              particlesCount,
		divisions:                   make([]division, divsCount),
		maxParticleBestUnchageCount: maxParticleBestUnchageCount,
		maxParticlesLength:          maxParticlesLength,
	}

	for i := uint(0); i < divsCount; i++ {
		parLen := maxParticlesLength * (i + 1) / divsCount
		swarm.divisions[i] = division{
			index:           i,
			particles:       makeDivision(particlesCount/divsCount, parLen),
			particlesLength: parLen,
		}
	}

	return swarm
}

func makeDivision(particlesCount, particleLength uint) []particle {
	log.Printf("making division. particles: %d particles length: %d", particlesCount, particleLength)
	population := make([]particle, particlesCount)

	wg := new(sync.WaitGroup)
	wg.Add(int(particlesCount))

	for i := 0; i < len(population); i++ {
		p := &population[i]
		go func() {
			p.position = rand.Float64s(particleLength)
			p.velocity = rand.Float64s(particleLength)
			p.renewExemplarCount = 0
			wg.Done()
		}()
	}

	wg.Wait()

	return population
}

// BestUnchangedCount returns swarm's global best unchanged count.
func (s *Swarm) BestUnchangedCount() uint {
	return s.bestUnchangedCount
}

// DivisionsCount returns number of divisions in swarm.
func (s *Swarm) DivisionsCount() int {
	return len(s.divisions)
}

// MaxParticlesLength returns maximum length of particles in swarm.
func (s *Swarm) MaxParticlesLength() uint {
	return s.maxParticlesLength
}

// UpdateFitness updates particles fitness
func (s *Swarm) UpdateFitness(dataset [][]float64, labels []string) {
	log.Printf("updating swarm fitness")
	wg := new(sync.WaitGroup)
	wg.Add(len(s.divisions))

	for i := 0; i < len(s.divisions); i++ {
		div := &s.divisions[i]
		go func() {
			log.Printf("updating division %d fitness", div.index)
			div.updateParticlesFitness(dataset, labels)
			log.Printf("division %d fitness updated.", div.index)
			wg.Done()
		}()
	}

	wg.Wait()
}

// UpdateParticlesPc updates learning probability (Pc) of all particles.
func (s *Swarm) UpdateParticlesPc() {
	log.Printf("updating swarm Pc")
	s.updateParticlesRank()

	for i := 0; i < len(s.divisions); i++ {
		div := s.divisions[i]
		for j := 0; j < len(div.particles); j++ {
			pc := 0.05 + 0.45*(math.Exp(float64(10*(div.particles[j].rank-1))/float64(s.particlesCount-1))/math.Expm1(10))
			div.particles[j].learningProbability = pc
		}
	}
	log.Printf("swarm particles pc updated.")
}

// AssignExemplars Assigns exemplars for all particles in divisions.
func (s *Swarm) AssignExemplars() {
	log.Printf("assigning exemplars for swarm particles")
	for i := 0; i < len(s.divisions); i++ {
		log.Printf("assigning exemplars for division: %d", s.divisions[i].index)
		s.divisions[i].assignExemplars(s.divisions[i:])
		log.Printf("exemplars for division: %d assigned.", s.divisions[i].index)
	}
}

// BestParticlePosition returns swarm's global best particle position.
func (s *Swarm) BestParticlePosition() []float64 {
	return s.bestParticle.position
}

// AnyParticleRenewExemplarCount returns true if cb returns true for any of particles renew exemplars passed into it.
func (s *Swarm) AnyParticleRenewExemplarCount(cb func(uint) bool) bool {
	for i := 0; i < len(s.divisions); i++ {
		for j := 0; j < len(s.divisions[i].particles); j++ {
			if cb(s.divisions[i].particles[j].renewExemplarCount) {
				return true
			}
		}
	}

	return false
}

// UpdateGlobalBest updates swarm-wide global best particle and update counter
func (s *Swarm) UpdateGlobalBest() {
	log.Printf("updating swarm global best particle")
	gbest := s.divisions[0].particles[0]
	for i := 0; i < len(s.divisions); i++ {
		for j := 0; j < len(s.divisions[i].particles); j++ {
			if p := s.divisions[i].particles[j]; p.fitness > gbest.fitness {
				gbest = p
			}
		}
	}

	if s.bestParticle == nil || gbest.fitness-s.bestParticle.fitness > math.Pow10(-int(config.Config().Optimization.GbestUnchangingPrecision)) {
		log.Printf("swarm global best was improved. resetting counter")
		s.bestUnchangedCount = 0
	} else {
		log.Printf("swarm global best was not improved. increasing counter")
		s.bestUnchangedCount++
	}
	s.bestParticle = &gbest
}

// BestParticleFitenss returns best particle fitness
func (s *Swarm) BestParticleFitenss() float64 {
	return s.bestParticle.fitness
}

// EvolveParticles evolves swarm particles.
func (s *Swarm) EvolveParticles(data [][]float64, labels []string) {
	for i := 0; i < len(s.divisions); i++ {
		for j := 0; j < len(s.divisions[i].particles); j++ {
			p := &s.divisions[i].particles[j]
			if p.renewExemplarCount == s.maxParticleBestUnchageCount {
				p.assignExemplar(uint(i), uint(j), s.divisions[i:])
			}
			p.updateVelocity(s.divisions)
			p.updatePosition()
			p.updateFitness(data, labels)
			s.UpdateGlobalBest()
		}
	}
}

// ChangeLength changes the length of divisions in the swarm.
func (s *Swarm) ChangeLength(dataset [][]float64, labels []string) {
	divsCount := uint(s.DivisionsCount())
	maxParticlesLength := s.maxParticlesLength

	bestDiv := &s.divisions[0]
	for _, div := range s.divisions {
		if fitness := div.getAverageFitness(); fitness > bestDiv.getAverageFitness() {
			bestDiv = &div
		}
	}
	bestLen := bestDiv.particlesLength
	if bestLen != maxParticlesLength {
		var k uint = 1
		for _, div := range s.divisions {
			if div.index == bestDiv.index {
				continue
			}

			newLen := bestLen * k / divsCount
			if div.particlesLength < newLen {
				div.particlesLength = newLen
				lenDiff := newLen - div.particlesLength
				for i := 0; i < len(div.particles); i++ {
					div.particles[i].position = append(div.particles[i].position, rand.Float64s(lenDiff)...)
					div.particles[i].velocity = append(div.particles[i].velocity, rand.Float64s(lenDiff)...)
					div.particles[i].updateFitness(dataset, labels)
					k++
				}
			} else if div.particlesLength > newLen {
				lenDiff := div.particlesLength - newLen
				for i := 0; i < len(div.particles); i++ {
					div.particles[i].position = append(div.particles[i].position[0:0:0], div.particles[i].position[:lenDiff]...)
					div.particles[i].velocity = append(div.particles[i].velocity[0:0:0], div.particles[i].velocity[:lenDiff]...)
					div.particles[i].updateFitness(dataset, labels)
					k++
				}
			}
		}
		s.UpdateParticlesPc()
		s.AssignExemplars()
	}
}

func (s *Swarm) updateParticlesRank() {
	log.Printf("updating swarm particles rank")
	particlesFitness := make([]particleFitness, 0, s.particlesCount)
	for i := 0; i < len(s.divisions); i++ {
		div := s.divisions[i]
		for j := 0; j < len(div.particles); j++ {
			particlesFitness = append(particlesFitness, particleFitness{
				divisionIndex:   uint(i),
				inDivisionIndex: uint(j),
				fitness:         div.particles[j].fitness,
			})
		}
	}
	sort.Sort(byFitness(particlesFitness))
	rank := s.particlesCount
	for i := uint(0); i < s.particlesCount; i++ {
		item := particlesFitness[i]
		s.divisions[item.divisionIndex].particles[item.inDivisionIndex].rank = rank
		rank--
	}
	log.Printf("swarm particles rank updated.")
}

type particleFitness struct {
	divisionIndex   uint
	inDivisionIndex uint
	fitness         float64
}

type byFitness []particleFitness

func (f byFitness) Len() int           { return len(f) }
func (f byFitness) Less(i, j int) bool { return f[i].fitness < f[j].fitness }
func (f byFitness) Swap(i, j int)      { f[i], f[j] = f[j], f[i] }
