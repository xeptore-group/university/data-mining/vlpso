package optimizer

import (
	"gitlab.com/xeptore-group/university/data-minings/vlpso/models"
)

// Optimizer represents VLPSO optimizer.
type Optimizer struct {
	swarm                    *models.Swarm
	divsCount                uint
	particlesCount           uint
	maxParticleBestUnchanged uint
	maxGlobalBestUnchaged    uint
}
