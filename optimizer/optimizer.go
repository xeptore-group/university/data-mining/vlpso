package optimizer

import (
	"fmt"
	"log"

	"gitlab.com/xeptore-group/university/data-minings/vlpso/config"
	"gitlab.com/xeptore-group/university/data-minings/vlpso/models"
)

// New returns a new instance of Optimizer.
func New(alpha, beta, divsCount, particlesCount uint) *Optimizer {
	if divsCount == 0 {
		panic("number of divisions must be greater than 0")
	}
	if particlesCount == 0 {
		panic("number of particles must be greater than 0")
	}

	return &Optimizer{
		divsCount:                divsCount,
		particlesCount:           particlesCount,
		maxGlobalBestUnchaged:    beta,
		maxParticleBestUnchanged: alpha,
	}
}

// Optimize executes optimizer on provided dataset.
func (o *Optimizer) Optimize(data [][]float64, labels []string) {
	if len(data) != len(labels) {
		panic(
			fmt.Errorf(
				"number of dataset rows (%d) is not equal with labels (%d)",
				len(data),
				len(labels),
			),
		)
	}

	particlesMaxLen := uint(len(data[0]))
	o.swarm = models.NewSwarm(
		o.divsCount,
		o.particlesCount,
		particlesMaxLen,
		o.maxParticleBestUnchanged,
	)
	o.swarm.UpdateFitness(data, labels)
	o.swarm.UpdateGlobalBest()
	o.swarm.UpdateParticlesPc()
	o.swarm.AssignExemplars()

	for i := 0; i < config.Config().Optimization.TotalEvolutionIterations; i++ {
		o.swarm.EvolveParticles(data, labels)
		if o.swarm.BestUnchangedCount() >= o.maxGlobalBestUnchaged {
			o.swarm.ChangeLength(data, labels)
		}

		if o.swarm.AnyParticleRenewExemplarCount(func(renewCount uint) bool {
			return renewCount >= o.maxParticleBestUnchanged
		}) {
			o.swarm.UpdateParticlesPc()
		}
	}

	log.Printf("Best particle position:\n%+v", o.swarm.BestParticlePosition())
	log.Printf("Best particle fitness: %f", o.swarm.BestParticleFitenss())
}
