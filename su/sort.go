package su

type featureSU struct {
	featureIndex int
	su           float64
}

type bySU []featureSU

func (s bySU) Len() int           { return len(s) }
func (s bySU) Less(i, j int) bool { return s[i].su < s[j].su }
func (s bySU) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
