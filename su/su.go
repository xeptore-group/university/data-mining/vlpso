package su

import (
	"fmt"
	"log"
	"math"
	"sort"
	"sync"

	"gitlab.com/xeptore-group/university/data-minings/vlpso/config"
)

func standardize(num float64) string {
	precision := "%."
	precision += fmt.Sprintf("%d", config.Config().SU.Precision)
	precision += "f"
	return fmt.Sprintf(precision, num)
}

func lablesCount(labels []string) map[string]int {
	out := make(map[string]int, len(labels))
	for _, label := range labels {
		if _, ok := out[label]; ok {
			out[label]++
		} else {
			out[label] = 1
		}
	}

	return out
}

func uniqueLabels(labels []string) []string {
	out := make([]string, 0, len(labels))
	for label := range lablesCount(labels) {
		out = append(out, label)
	}

	return append(out[0:0:0], out...)
}

func featuresCount(features []float64) map[string]int {
	out := make(map[string]int, len(features))
	for _, feature := range features {
		if _, ok := out[standardize(feature)]; ok {
			out[standardize(feature)]++
		} else {
			out[standardize(feature)] = 1
		}
	}

	return out
}

func uniqueFeatures(features []float64) []string {
	out := make([]string, 0, len(features))
	for feature := range featuresCount(features) {
		out = append(out, feature)
	}

	return append(out[0:0:0], out...)
}

func featureInfo(feature string, featureInstances []float64, labels []string, uniqueizedLabels []string) []int {
	if len(labels) != len(featureInstances) {
		panic("length of labels should be equal with length of feature instances")
	}

	featureLabelFreq := make(map[string]int, len(uniqueizedLabels))
	for i, f := range featureInstances {
		if standardize(f) != feature {
			continue
		}
		if _, ok := featureLabelFreq[labels[i]]; ok {
			featureLabelFreq[labels[i]]++
		} else {
			featureLabelFreq[labels[i]] = 1
		}
	}

	out := make([]int, 0, len(uniqueizedLabels))
	for _, freq := range featureLabelFreq {
		out = append(out, freq)
	}

	return out
}

func info(params ...int) float64 {
	sum := 0
	for _, param := range params {
		sum += param
	}

	result := 0.0
	for _, param := range params {
		if param <= 0 {
			continue
		}
		p := float64(param) / float64(sum)
		result += -(p * math.Log10(p))
	}

	return result
}

func featureEntropy(column []float64) float64 {
	featuresFrequency := featuresCount(column)

	total := 0
	for _, count := range featuresFrequency {
		total += count
	}

	result := 0.0

	for _, count := range featuresFrequency {
		p := (float64(count) / float64(total))
		result += -p * math.Log10(p)
	}

	return result
}

func labelsEntropy(labels []string) float64 {
	labelsFrequency := lablesCount(labels)

	total := 0
	for _, count := range labelsFrequency {
		total += count
	}

	result := 0.0

	for _, count := range labelsFrequency {
		p := (float64(count) / float64(total))
		result += -p * math.Log10(p)
	}

	return result
}

func hfc(featureInstances []float64, labels []string) float64 {
	if len(featureInstances) != len(labels) {
		panic(fmt.Errorf("length of feature instances (%d) and labels (%d) must be equal", len(featureInstances), len(labels)))
	}

	uniqueizedFeatures := uniqueFeatures(featureInstances)
	table := make(map[string][]int, len(uniqueizedFeatures))
	uniqueizedLabels := uniqueLabels(labels)
	for _, f := range uniqueizedFeatures {
		table[f] = featureInfo(f, featureInstances, labels, uniqueizedLabels)
	}

	totalSum := 0
	for _, f := range table {
		sum := 0
		for _, v := range f {
			sum += v
		}
		totalSum += sum
	}

	result := 0.0
	for _, f := range table {
		sum := 0
		for j := 0; j < len(f); j++ {
			sum += f[j]
		}
		result += float64(sum) / float64(totalSum) * info(f...)
	}

	return result
}

func sortedFeaturesIndex(dataset [][]float64, labels []string) []int {
	log.Printf("started sorting dataset features...")
	wg := new(sync.WaitGroup)
	sus := make([]featureSU, len(dataset[0]))
	wg.Add(len(sus))
	log.Printf("started calculating features SU...")
	for i := 0; i < len(sus); i++ {
		i := i
		go func() {
			column := make([]float64, len(dataset))
			for j := 0; j < len(dataset); j++ {
				column[j] = dataset[j][i]
			}
			su := (featureEntropy(column) - hfc(column, labels)) / (featureEntropy(column) + labelsEntropy(labels))
			sus[i] = featureSU{
				featureIndex: i,
				su:           su,
			}
			log.Printf("feature %d SU calculated.", i+1)
			wg.Done()
		}()
	}
	wg.Wait()
	sort.Sort(sort.Reverse(bySU(sus)))
	out := make([]int, len(sus))
	for i, su := range sus {
		out[i] = su.featureIndex
	}
	log.Printf("sorting dataset features finished")
	return out
}

// RearrangeDataset returns rearranged version of dataset based on feature correlations in SU.
func RearrangeDataset(dataset [][]float64, labels []string) [][]float64 {
	log.Printf("started rearranging dataset...")
	sortedFeaturesIndecies := sortedFeaturesIndex(dataset, labels)
	rearrangedDataset := make([][]float64, len(dataset))
	for i := 0; i < len(dataset); i++ {
		rearrangedDataset[i] = make([]float64, len(dataset[i]))
		for j := 0; j < len(sortedFeaturesIndecies); j++ {
			rearrangedDataset[i][j] = dataset[i][sortedFeaturesIndecies[j]]
		}
	}
	log.Printf("dataset rearranged")
	return rearrangedDataset
}
