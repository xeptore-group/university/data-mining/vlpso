package su

import (
	"fmt"
	"sort"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestLabelsCount(t *testing.T) {
	Convey("Given a list of labels", t, func() {
		Convey("It should return expected labels with their quantity", func() {
			labels := []string{"2", "3", "1", "3", "3", "2", "1", "1", "3", "2", "2", "2", "3", "3"}
			lc := lablesCount(labels)
			So(lc, ShouldResemble, map[string]int{
				"1": 3,
				"2": 5,
				"3": 6,
			})
		})
	})
}

func TestUniqueLabels(t *testing.T) {
	Convey("Given a list of labels", t, func() {
		Convey("It should return uniqueized list of labels", func() {
			labels := []string{"2", "3", "1", "3", "3", "2", "1", "1", "3", "2", "2", "2", "3", "3"}
			lc := uniqueLabels(labels)
			sort.Strings(lc)
			So(lc, ShouldResemble, []string{"1", "2", "3"})
		})
	})
}

func TestFeaturesCount(t *testing.T) {
	Convey("Given a list of feature instances", t, func() {
		Convey("It should return expected features with their quantity", func() {
			features := []float64{5.0, 9.0, 10.0, 9.0, 5.0, 9.0, 10.0, 9.0, 10.0, 10.0, 10.0, 5.0, 5.0, 9.0}
			fc := featuresCount(features)
			So(fc, ShouldResemble, map[string]int{
				"5":  4,
				"9":  5,
				"10": 5,
			})
		})
	})
}

func TestUniqueFeatures(t *testing.T) {
	Convey("Given a list of features", t, func() {
		Convey("It should return uniqueized list of features", func() {
			features := []float64{5.0, 9.0, 10.0, 9.0, 5.0, 9.0, 10.0, 9.0, 10.0, 10.0, 10.0, 5.0, 5.0, 9.0}
			fc := uniqueFeatures(features)
			sort.Strings(fc)
			So(fc, ShouldResemble, []string{"10", "5", "9"})
		})
	})
}

func TestFeatureInfo(t *testing.T) {
	Convey("Given a list of features and labels", t, func() {
		Convey("It should panic if length of feature instances if different from length of labels", func() {
			features := []float64{5.0, 9.0, 10.0, 9.0, 5.0, 9.0, 10.0, 9.0, 10.0, 10.0, 10.0, 5.0, 9.0}
			labels := []string{"2", "3", "1", "3", "3", "2", "1", "1", "3", "2", "2", "2", "3", "3"}
			So(func() {
				featureInfo("5", features, labels, uniqueLabels(labels))
			}, ShouldPanic)
		})
		Convey("It should return uniqueized list of features", func() {
			features := []float64{5.0, 9.0, 10.0, 9.0, 5.0, 9.0, 10.0, 9.0, 10.0, 10.0, 10.0, 5.0, 5.0, 9.0}
			labels := []string{"2", "3", "1", "3", "3", "2", "1", "1", "3", "2", "2", "2", "3", "3"}
			info := featureInfo("5", features, labels, uniqueLabels(labels))
			So(info, ShouldResemble, []int{2, 2})
		})
	})
}

func TestInfo(t *testing.T) {
	Convey("Given a list of feature information", t, func() {
		Convey("It should return total information of that feature", func() {
			i := info(1, 3, 1)
			So(fmt.Sprintf("%.10f", i), ShouldEqual, "0.4126972515")
		})
	})
}

func TestFeatureEntropy(t *testing.T) {
	Convey("Given a list of feature instances", t, func() {
		Convey("It should calculate entropy properly", func() {
			features := []float64{5.0, 9.0, 10.0, 9.0, 5.0, 9.0, 10.0, 9.0, 10.0, 10.0, 10.0, 5.0, 5.0, 9.0}
			e := featureEntropy(features)
			So(fmt.Sprintf("%.10f", e), ShouldEqual, "0.4748466065")
		})
	})
}

func TestLabelsEntropy(t *testing.T) {
	Convey("Given a list of labels", t, func() {
		Convey("It should calculate entropy properly", func() {
			labels := []string{"2", "3", "1", "3", "3", "2", "1", "1", "3", "2", "2", "2", "3", "3"}
			e := labelsEntropy(labels)
			So(fmt.Sprintf("%.10f", e), ShouldEqual, "0.4607622294")
		})
	})
}

func TestConditionalEntropy(t *testing.T) {
	Convey("Given a list of feature instances and their labels", t, func() {
		Convey("It should panic when length of feature instances and labels differs", func() {
			features := []float64{5.0, 9.0, 10.0, 9.0, 5.0, 9.0, 10.0, 9.0, 10.0, 10.0, 10.0, 5.0, 5.0}
			labels := []string{"2", "3", "1", "3", "3", "2", "1", "1", "3", "2", "2", "2", "3", "3"}
			So(func() {
				hfc(features, labels)
			}, ShouldPanic)
		})
		Convey("It should calculate conditional entropy properly", func() {
			features := []float64{5.0, 9.0, 10.0, 9.0, 5.0, 9.0, 10.0, 9.0, 10.0, 10.0, 10.0, 5.0, 5.0, 9.0}
			labels := []string{"2", "3", "1", "3", "3", "2", "1", "1", "3", "2", "2", "2", "3", "3"}
			e := hfc(features, labels)
			So(fmt.Sprintf("%.10f", e), ShouldEqual, "0.3970240199")
		})
	})
}

func TestSortedFeaturesIndex(t *testing.T) {
	Convey("Given a dataset and labels", t, func() {
		Convey("It should return sorted feature indecies properly", func() {
			dataset := [][]float64{
				[]float64{5.0, 9.0},
				[]float64{9.0, 9.0},
				[]float64{10.0, 9.0},
				[]float64{9.0, 9.0},
				[]float64{5.0, 10.0},
				[]float64{9.0, 5.0},
				[]float64{10.0, 9.0},
				[]float64{9.0, 3.8},
				[]float64{10.0, 2.2},
				[]float64{10.0, 0.9},
				[]float64{10.0, 3.6},
				[]float64{5.0, 5.4},
				[]float64{5.0, 4.5},
				[]float64{9.0, 2.9},
			}
			labels := []string{"2", "3", "1", "3", "3", "2", "1", "1", "3", "2", "2", "2", "3", "3"}
			// col: 0 -> 0.08317855022401945
			// col: 1 -> 0.4004802483767513
			sfi := sortedFeaturesIndex(dataset, labels)
			So(sfi, ShouldResemble, []int{1, 0})
		})
	})
}

func TestRearrangeDataset(t *testing.T) {
	Convey("Given a dataset and labels", t, func() {
		Convey("It should return dataset with reaaranged features", func() {
			dataset := [][]float64{
				[]float64{5.0, 9.0},
				[]float64{9.0, 9.0},
				[]float64{10.0, 9.0},
				[]float64{9.0, 9.0},
				[]float64{5.0, 10.0},
				[]float64{9.0, 5.0},
				[]float64{10.0, 9.0},
				[]float64{9.0, 3.8},
				[]float64{10.0, 2.2},
				[]float64{10.0, 0.9},
				[]float64{10.0, 3.6},
				[]float64{5.0, 5.4},
				[]float64{5.0, 4.5},
				[]float64{9.0, 2.9},
			}
			labels := []string{"2", "3", "1", "3", "3", "2", "1", "1", "3", "2", "2", "2", "3", "3"}
			rearrangedDataset := RearrangeDataset(dataset, labels)
			So(rearrangedDataset, ShouldResemble, [][]float64{
				[]float64{9.0, 5.0},
				[]float64{9.0, 9.0},
				[]float64{9.0, 10.0},
				[]float64{9.0, 9.0},
				[]float64{10.0, 5.0},
				[]float64{5.0, 9.0},
				[]float64{9.0, 10.0},
				[]float64{3.8, 9.0},
				[]float64{2.2, 10.0},
				[]float64{0.9, 10.0},
				[]float64{3.6, 10.0},
				[]float64{5.4, 5.0},
				[]float64{4.5, 5.0},
				[]float64{2.9, 9.0},
			})
		})
	})
}
